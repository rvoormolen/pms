$posfile = "C:\Path\To\LastPos.ini

$server_url = "https://www.example.com/pokedop/"
$secret     = "$secret in web/config.php"

$ie = New-Object -ComObject "InternetExplorer.Application"
$ie.visible = $true

while ($true) {
    $pos = $(Get-Content $posfile).Split(':')
    $lat = $pos[0]
    $lon = $pos[1]
    White-Host $pos
    $url = "Sserver_url/post.php?secret=$secret&lat=$lat&lon=$lon"
    $ie.Navigate($url)
    Start-Sleep -Seconds 20
}
