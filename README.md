# PMS
PMS is a self-hosted, mobile-ready mapping tool for NecroBot, the leading open source Pokemon GO Bot.

### SECURITY NOTICE
This code has been written as a joke, no security testing has been done and we
stronly advise to check the code yourself before continue

### Version
0.1.0 (`Pokeball One-Zero-One`)

## Installation
### Webserver
**You need to install a PMS webinterface per Client!**
```sh
$ cd /tmp
$ git clone https://path.to.git.repo/ PMS
$ cp -prv PMS/web /var/www/HTML/pms
$ vim /var/www/HTML/pms/config.php
```

### Client

**Make sure you have NecroBot installed!**

```powershell
$ wget https://path.to.git.raw/client/PMS.ps1 -OutFile "$([Environment]::GetFolderPath("Desktop"))\PMS.ps1"
$ notepad "$([Environment]::GetFolderPath("Desktop"))\PMS.ps1"
# Adjust the settings
$ "$([Environment]::GetFolderPath("Desktop"))\PMS.ps1"
# Or Run PMS.ps1 from your desktop
```
