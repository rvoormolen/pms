<?php
    require("config.php");

    $json = array();
    if($result = $mysqli->query("SELECT * FROM locations ORDER BY datetime DESC LIMIT 100")) {
        while($row = $result->fetch_array()) {
            if(is_numeric($row['lat']) && is_numeric($row['lon'])) {
                $record = array('datetime' => $row['datetime'], 'lat' => $row['lat'], 'lon' => $row['lon'] );
                $json[] = $record;
            }
        }
        echo json_encode($json);
    } else {
        printf("Errormessage: %s\n", $mysqli->error);
    }


        #header(sprintf("location: http://www.openstreetmap.org?mlat=%s&mlon=%s&zoom=14", $lat, $lon));
