<?php
    require('config.php');

    if (!isset($_REQUEST['secret']) || $_REQUEST['secret'] !== $secret) {
        header("HTTP/1.1 403 Pleur op!");
    }

    $lat = $_REQUEST['lat'];
    $lon = $_REQUEST['lon'];

    if($stmt = $mysqli->prepare("INSERT INTO locations(datetime, lat, lon) VALUES (NOW(), ?, ?)")) {
        $stmt->bind_param("dd", $lat, $lon);
        $stmt->execute();
    } else {
        printf("Errormessage: %s\n", $mysqli->error);
    }
