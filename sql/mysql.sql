--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `datetime` datetime DEFAULT NULL,
  `lat` double(12,8) DEFAULT NOT NULL,
  `lon` double(12,8) DEFAULT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
